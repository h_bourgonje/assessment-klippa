package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"sync"

	"github.com/fsnotify/fsnotify"
	"github.com/joho/godotenv"
)

var (
	cmd                      = flag.NewFlagSet("", flag.ExitOnError)
	apiKey           *string = cmd.String("register", "", "Your registration key for the Klippa API.")
	template         *string = cmd.String("template", "", "What template to use. Choose between identity and financial_full")
	extraction       *string = cmd.String("extraction", "", "The type of extraction to use, choose between full or fast.")
	source           *string = cmd.String("source", "", "The file or folder to send to the API.")
	save             *bool   = cmd.Bool("save-output", true, "Wheter or not to save the results to (a) JSON file(s).")
	useEnv           *bool   = cmd.Bool("use-env-file", false, "When true, the .env file of the project will be used, to be configured in the project root folder.")
	monitorDirectory *bool   = cmd.Bool("monitor-directory", false, "When true, the folder given at 'source' will be monitored for new files.")
)

type ParseOptions struct {
	ApiKey           string
	Template         string
	Extraction       string
	Source           string
	Save             bool
	EnvEnabled       bool
	MonitorDirectory bool
}

func main() {
	cmd.Parse(os.Args[1:])

	parseOptions := ParseOptions{
		ApiKey:           *apiKey,
		Template:         *template,
		Extraction:       *extraction,
		Source:           *source,
		Save:             *save,
		EnvEnabled:       *useEnv,
		MonitorDirectory: *monitorDirectory,
	}

	if *useEnv {
		err := godotenv.Load(".env")
		LogIfErr(err)

		saveBool, err := strconv.ParseBool(os.Getenv("SAVE"))
		LogIfErr(err)

		monitorDirectoryBool, err := strconv.ParseBool(os.Getenv("MONITOR_DIRECTORY"))
		LogIfErr(err)

		parseOptions.ApiKey = os.Getenv("API_KEY")
		parseOptions.Template = os.Getenv("TEMPLATE")
		parseOptions.Extraction = os.Getenv("EXTRACTION")
		parseOptions.Source = os.Getenv("SOURCE")
		parseOptions.Save = saveBool
		parseOptions.EnvEnabled = *useEnv
		parseOptions.MonitorDirectory = monitorDirectoryBool
	}
	validate(&parseOptions)
	process(&parseOptions)
}

// Validate the input begore parsing the file(s).
func validate(parseOptions *ParseOptions) {
	if parseOptions.ApiKey != "" && parseOptions.Template != "" && parseOptions.Extraction != "" && parseOptions.Source != "" {
		return
	}

	methodSpecificText := "the input flags"
	if parseOptions.EnvEnabled {
		methodSpecificText = "the env file"
	}

	fmt.Println("One or more arguments are missing from " + methodSpecificText + ", check below for more information:")
	flag.PrintDefaults()
	os.Exit(1)
}

func process(parseOptions *ParseOptions) {
	if isDirectory(&parseOptions.Source) {
		if parseOptions.MonitorDirectory {
			directoryMonitor(parseOptions)
		}
		processConcurrently(parseOptions)
	} else {
		base64Encoding := fileToBase64(&parseOptions.Source)
		parseDocument(parseOptions, &base64Encoding)
	}
}

func processConcurrently(parseOptions *ParseOptions) {
	files, err := ioutil.ReadDir(parseOptions.Source)
	LogIfErr(err)

	var wg sync.WaitGroup
	wg.Add(len(files))

	for i := 0; i < len(files); i++ {
		go func(i int) {
			defer wg.Done()
			file := files[i]
			location := parseOptions.Source + "/" + file.Name()
			base64Encoding := fileToBase64(&location)
			parseDocument(parseOptions, &base64Encoding)
		}(i)
	}
	wg.Wait()
}

// Method used for monitoring the given folder.
func directoryMonitor(parseOptions *ParseOptions) {
	watcher, err := fsnotify.NewWatcher()
	LogIfErr(err)
	defer watcher.Close()

	done := make(chan bool)
	processConcurrently(parseOptions)
	go func() {
		for {
			select {
			// On a create event, parse the newly created file.
			case event := <-watcher.Events:
				if event.Op.String() == "CREATE" {
					base64Encoding := fileToBase64(&event.Name)
					parseDocument(parseOptions, &base64Encoding)
				}
			case err := <-watcher.Errors:
				LogIfErr(err)
			}
		}
	}()
	if err := watcher.Add(parseOptions.Source); err != nil {
		LogIfErr(err)
	}
	<-done
}

func isDirectory(source *string) bool {
	file, err := os.Open(*source)
	LogIfErr(err)

	fileInfo, err := file.Stat()
	LogIfErr(err)

	return fileInfo.IsDir()
}

func fileToBase64(file *string) string {
	bytes, err := ioutil.ReadFile(*file)
	LogIfErr(err)

	var base64Encoding string
	base64Encoding += toBase64(bytes)
	return base64Encoding
}

func toBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func LogIfErr(err error) {
	if err != nil {
		log.Printf("error occurred: %s", err)
	}
}
