package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

const baseUrl = "https://custom-ocr.klippa.com/api/v1/"

type RequestBody struct {
	Document            string `json:"document"`
	Template            string `json:"template"`
	Pdf_text_extraction string `json:"pdf_text_extraction"`
}

func parseDocument(parseOptions *ParseOptions, document *string) {
	requestBody := RequestBody{Document: *document, Template: parseOptions.Template, Pdf_text_extraction: parseOptions.Extraction}
	var buf = new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.Encode(requestBody)

	req, err := http.NewRequest("POST", baseUrl+"parseDocument", buf)
	LogIfErr(err)

	req.Header.Set("X-Auth-Key", parseOptions.ApiKey)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(req)
	LogIfErr(err)
	defer res.Body.Close()

	// Format and print the result of the processing
	responseBody, err := ioutil.ReadAll(res.Body)
	LogIfErr(err)
	var result map[string]interface{}
	json.Unmarshal([]byte(responseBody), &result)
	fmt.Println("Result:", result["result"])

	// If this is true, save it to a file, named with the content of the string
	if parseOptions.Save {
		out, err := os.Create("./extracted_file/extracted_data-" + time.Now().Format(time.RFC3339Nano) + ".json")
		LogIfErr(err)
		io.Copy(out, bytes.NewBuffer(responseBody))
	}
}
