# Assessment Klippa

Bij deze de afgeronde versie van de assessment. Hier leg ik uit hoe de CLI te gebruiken is. 

**Uitvoeren vanaf de commandline:**

Hier zijn de volgende arguments voor te gebruiken:
- -register: hier zet je je API key neer.
- -template: hier kies je wel template je wilt gebruiken.
- -extraction: hier kies je de manier van extraction: full of fast
- -source: hier zet je het de bron van parsing neer. Dit kan zowel een map zijn als een enkel bestand. Meerdere bestanden worden concurrent uitgevoerd.
- -monitor-directory: wanneer deze true is, wordt de map die je bij source hebt opgegeven gemonitord op nieuwe files.

Standaard wordt de output opgeslagen in json formaat, mocht je dit niet willen kan je deze als false meegeven.

Bijvoorbeeld:

`./klippa-ocr -register *************** -template financial_full -extraction fast -source helper_files/ -monitor-directory true`

**Uitvoeren vanuit een .env file**

Er zit een .env file in project root. Met onderstaand commando kan je deze CLI tool aanroepen met informatie uit deze file:

`./klippa-ocr -use-env-file true`
